package com.rewardsnetwork.dressing.exception;

/**
 * Used to stop processing; need not be caught.
 * 
 * @author greg trester
 */
public final class RulesViolation extends RuntimeException {
  
}

