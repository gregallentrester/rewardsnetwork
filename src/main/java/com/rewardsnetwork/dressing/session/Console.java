package com.rewardsnetwork.dressing.session;

import java.util.Scanner;

import com.rewardsnetwork.dressing.exception.RulesViolation;
import java.util.NoSuchElementException;

import com.rewardsnetwork.dressing.validation.Constraints;


/**
 * This demo application is responsible for the following operations,
 * which model the procedural steps to dress for the weather:
 *   <ul>
 *     <li> Process a list of commands    </li>
 *     <li> Enforce related rules         </li>
 *     <li> Display appropriate output    </li>
 *   </ul>
 *
 *  Plausible/valid/ legal Inputs are as follows:
 *
 *    A Climate type   [ HOT | COLD ]
 *    A Comma-separated list of numeric commands
 *
 * The Business Rules are as follows:
 *
 * <ul>
 *   <li> Start in the house with your PJ’s on                  </li>
 *   <li> Pajamas must be taken off first                       </li>
 *   <li> Only 1 piece of each type of clothing may be put on   </li>
 *   <li> You cannot put on socks when it is hot                </li>
 *   <li> You cannot put on a jacket when it is hot             </li>
 *   <li> Socks must be put on before footwear
 *        (either: socks, pants | pants, socks)                 </li>
 *   <li> Pants before footwear                                 </li>
 *   <li> Shirt before the headwear or jacket                   </li>
 *   <li>
 *     You cannot leave the house until all items of clothing
 *     are on (except socks and a jacket when it’s hot)         </li>
 *   <li>
 *    If an invalid command is issued, respond with "FAIL"
 *    and stop processing commands (fast-fail abend)           </li>
 * </ul>
 *
 * @see <a href='https://binged.it/2JqdG2z'>Atom</a>
 *
 * @author greg trester
 */
public final class Console {

  private final Constraints rules;

  // "poison pill" that ends session
  private static final String QUIT = "Q";

  public Console() {
    rules = new Constraints();
  }

  /**
   * The canonical entry-point into a Java application.
   *
   * @param args the formal command line arguments
   */
  public static void main(String [] args) {
    new Console().accept();
  }

  /**
   * Instantiate an instance of Scanner, start accepting requests.
   */
  public void accept() {

    System.out.println("\n Awaiting ... \n");
    
    String ask;
    Scanner sc = new Scanner(System.in);
    
    try {

      while (true) {

        ask = sc.next();
        
        if (ask.equalsIgnoreCase(QUIT)) {
          break;
        }

        rules.handleAsk(ask);
      }
    }
    catch(NoSuchElementException | RulesViolation e) {
      sc.close();
      System.exit(-9);
    }
    finally {

      rules.reportJournaledEvents();

      sc.close();
      System.exit(0);
    }
  }
}