package com.rewardsnetwork.dressing.session;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;

import com.rewardsnetwork.dressing.model.Catalog;
import com.rewardsnetwork.dressing.exception.RulesViolation;


/**
 * Encapsulates an ordered list of events/strings;
 * insertion logic compensates for the allowance of
 * duplicates in a <code>List</code> (by rejecting
 * any/all duplicates); this is a <i>StateMachine</i>
 * (holds events at-rest).
 */
public final class Journal {

  /**
   * Ordered list of events; insertion logic compensates
   * for the allowance of duplicates (by rejecting them).
   */
  private final LinkedList<String> events = new LinkedList<>();


  /**
   * When <code>true</code> the report will include
   * a FAIL signal is appended to the Output statement.
   */
  private boolean errorState;

  private static final String HOT =
    Climate.ValidStates.HOT.name();


  private static final String COLD =
    Climate.ValidStates.COLD.name();


  /**
   * Set an error condition (to <code>true</code>) that will be interrogated
   * at report time and interpreted as a <i>FAIL</i>, resulting appending a
   * <i>FAIL</i> string to the <i>Output</i> statement, at the completion of
   * the application's processing.
   *
   * @see #addEvent(String)
   * @see #publish()
   * @see #containsCoreEvents()
   * 
   * @see com.rewardsnetwork.demo.validation.Constraints#handleAsk(String)
   * @see com.rewardsnetwork.demo.validation.Constraints#errorOnRepeat(String)
   * @see com.rewardsnetwork.demo.validation.Constraints#assureThisHappened(String)
   */
  public final void flagErrorState() {
    errorState = Boolean.TRUE;
  }


  /**
   * Set an error condition (to <code>true</code>) that will be interrogated
   * at report time and interpreted as a <i>FAIL</i>, resulting appending a
   * <i>FAIL</i> string to the <i>Output</i> statement, at the completion of
   * the application's processing.
   */
  private final boolean hasErrors() {
    return errorState;
  }

  
  /**
   * Journal the stringified events.
   *
   * @param event the stringified event to be journaled
   * @return whether the stringified event was successfully journaled
   */
  public final boolean addEvent(String event) {

    if (events.contains(event)) {

      flagErrorState();
      publish();
      throw new RulesViolation();
    }
    
    events.add(event);

    return Boolean.TRUE;
  }

  
  /**
   * Publish the event model (which were captured in th order of their
   * occurrence) for use by a collaborating class that contains logic
   * that enforces certain rules.
   *
   * @return an unmodifiable <code>ArrayList</code>
   */
  public final List publishedEvents() {
    return Collections.unmodifiableList(events);
  }


  /**
   * Determine whether the internal event model
   * already contains the posited event/command
   * 
   * @param cmdID the event being checked to be a member of the internal model
   * @return whether event/command is in the internal model
   */
  public final boolean contains(String cmdID) {
    return events.contains(cmdID);
  }


  /**
   * The ambient temperature/validation context.
   */
  private String climate = null;

  
  /**
   * Return the ambient temperature (the validation context)
   *
   * @return the ambient temperature, either <i>HOT</i> | <i>COLD</i>
   */
  public String getClimate() {
    return climate;
  }


  /**
   * Set the context/ambient temperature EXACTLY ONCE.
   *
   * @param value ambient temperature, either (<i>HOT</i> | <i>COLD</i>)
   */
  public void setClimate(String value) {
    climate = (climate == null) ? value: climate;
  }

  
  /**
   * Translate eventIDs into intelligible notifications.
   *
   * @see com.rewardsnetwork.demo.validation.Constraints#reportJournaledEvents()
   */
  public final void publish() {

    Catalog catalog = new Catalog();

    StringBuilder sbReport =
      new StringBuilder("\n Output ");

    for (String event : events) {

      if (climate.equalsIgnoreCase(HOT)) {

        if ( ! incurredInfractions(event)) {

          sbReport.append(
            catalog.lookup(event).getHotResponse()).
            append(" ");
        }
        else {
          flagErrorState();
        }
      }
      else if (climate.equalsIgnoreCase(COLD)) {

        if ( ! incurredInfractions(event)) {

          sbReport.append(
            catalog.lookup(event).getColdResponse()).
            append(" ");
        }
        else {
          flagErrorState();
        }
      }
    }

    String addendum = (hasErrors()) ? "FAIL" : " ";
    System.out.println(sbReport + addendum + "\n ...\n\n");
  }
  

  /**
   * Check whether cumulatively, there were validation infractions.
   *
   * @param event the event - value: String "7" - that provides the
   *              context in which to check whether cumulatively,
   *              there were validation infractions during the session.
   *
   * @return whether cumulatively, there were validation infractions.
   */
  private boolean incurredInfractions(String event) {

    if ((event.equals("7") &&
         ! hasErrors() &&
         containsCoreEvents()) ||
         ( ! event.equals("7"))) {
      return Boolean.FALSE;
    }
    
    return Boolean.TRUE;
  }


  /**
   * Determine whether the <code>Journal</code>'s model contains the requisite
   * (as per specs) correct/legal instances of commands, which are being modeled
   * as instances of the class:
   *
   *   <blockquote>../model.Ask.java</blockquote>
   *
   * For a HOT climate, The requisite elements are:
   *
   *   <blockquote>
   *     [0] -> [8] -> [1]  -> [2]  -> [4] -> [7]
   *   </blockquote>
   *
   * For a COLD climate, The requisite elements are:
   *
   *   <blockquote>
   *     [0] -> [8] -> [6] -> [3]  -> [4] -> [2] -> [5] -> [1] -> [7]
   *   </blockquote>
   *
   * @return whether the <code>Journal</code> contains the requisite
   *         (as per specs) correct/legal posited commands
   *
   * @see #incurredInfractions(String)
   */
  private boolean containsCoreEvents() {

    boolean isComprehensive = Boolean.FALSE;
    
    if (climate.equalsIgnoreCase(COLD)) {

      isComprehensive =
        (events.contains("0")) &&
        (events.contains("8")) &&
        (events.contains("6")) &&
        (events.contains("3")) &&
        (events.contains("4")) &&
        (events.contains("2")) &&
        (events.contains("5")) &&
        (events.contains("1")) &&
        (events.contains("7"));
    }
    else if (climate.equalsIgnoreCase(HOT)) {

      isComprehensive =
        (events.contains("0")) &&
        (events.contains("8")) &&
        (events.contains("6")) &&
        (events.contains("4")) &&
        (events.contains("2")) &&
        (events.contains("1")) &&
        (events.contains("7"));
    }
    
    if ( ! isComprehensive) {
      flagErrorState();
    }

    return isComprehensive;
  }


  /**
   * An <code>enum</code> wrapper that provides
   * an affordance for any future computations.
   *
   * @author greg trester
   */
  public static final class Climate {

    /**
     * An <code>enum</code> of acceptable climate states.
     */
    public enum ValidStates {
      HOT, COLD
    }
  }
}


