package com.rewardsnetwork.dressing.model;

import java.util.concurrent.ConcurrentHashMap;

/**
 * A collection of instances of the <code>Ask</code> class is used by the
 * <code>Constraints</code> to validate incoming dressing activities;
 * there's a <code>static</code> initializer block in the <code>Catalog</code>
 * class, which instantiates and holds instances of the <code>Constraints</code>
 * class.
 *
 * NB  This class should ideally be <code>final</code>; however, since Mockito 
 *     cannot mock/spy the following, this class remains non-final:
 * <ul>
 *   <li> final classes       </li>
 *   <li> anonymous classes   </li>
 *   <li> primitive types     </li>
 * </ul>
 * 
 * @author greg trester
 */
public class Catalog {
  
  public static final String NOT_FOUND = "NOT FOUND";

  public static final int PJON = 0;
  public static final int FOOTWEAR = 1;
  public static final int HEADWEAR = 2;
  public static final int SOCKS = 3;
  public static final int SHIRT = 4;
  public static final int JACKET = 5;
  public static final int PANTS = 6;
  public static final int LEAVE = 7;
  public static final int PJOFF = 8;

  /**
   * The <code>ConcurrentHashMap</code>, a collection of <code>Ask</code>s which
   * embody the responses for a particular user choice - subject to validation.
   */
  private static final ConcurrentHashMap<Integer,Ask> COMMANDS =
    new ConcurrentHashMap<>();
  
  /*
    Populate the internal <code>ConcurrentHashMap</code> collection w/ several
    Asks; the model's elements which enclose a description, and 2 options based
    on conditions (all hard-encoded, as per specs).
   */
  static {

    COMMANDS.put(
      PJON,
      new Ask(PJON, "In house with PJs on", "", ""));

    COMMANDS.put(
      FOOTWEAR,
      new Ask(FOOTWEAR, "Put on footwear", "sandals", "boots"));

    COMMANDS.put(
      HEADWEAR,
      new Ask(HEADWEAR, "Put on headwear", "sunglasses", "hat"));

    COMMANDS.put(
      SOCKS,
      new Ask(SOCKS, "Put on socks", "FAIL", "socks"));

    COMMANDS.put(
      SHIRT,
      new Ask(SHIRT, "Put on shirt", "shirt", "shirt"));

    COMMANDS.put(
      JACKET,
      new Ask(JACKET, "Put on jacket", "FAIL", "jacket"));

    COMMANDS.put(
      PANTS,
      new Ask(PANTS, "Put on pants", "shorts", "pants"));

    COMMANDS.put(
      LEAVE,
      new Ask(LEAVE, "leave house", "leaving house", "leaving house"));

    COMMANDS.put(
      PJOFF,
      new Ask(PJOFF, "Take off pajamas", "Removing PJs", "Removing PJs"));
  }

  /**
   * Look-up/retrieve/return an instance of a <code>Ask</code>, found based
   * upon matching a user-supplied value with the <code>Ask</code>'s integral,
   * scalar key value.
   *
   * @param cmdkey the integer value that signifies the (business-encoded) index 
   *               of a <code>Ask</code> which embodies a command line ask
   *
   * @return a reference to an activity which is embodied as an instance of the
   *         <code>com.rewardsnetwork.demo.domain.Ask</code> class; found
   *         via an index in a <code>ConcurrentHashMap</code>; <code>null</code> 
   *         is returned if the <code>cmdkey</code> is not found
   *
   * @see com.rewardsnetwork.demo.validation.Constraints#handleAsk(String) 
   *      
   * @see com.rewardsnetwork.demo.session.Journal#publish()
   */
  public final Ask lookup(String cmdkey) {
    return COMMANDS.get(Integer.parseInt(cmdkey));
  }
}
