package com.rewardsnetwork.dressing.model;

/**
 *
 * Immutable representation of one end-user-issued <code>Ask</code>;
 * Instances of this class are interrogated by an instance of the
 * <code>Constraints</code> class to identify and validate
 * the numerically-encoded <i>Activity/Ask</i>; The numericID
 * of any such user <i>Activity/Ask</i> serves (by an ordained
 * convention/specification) as an indice into a collection, which
 * is modeled by an instance of the <code>Catalog</code>
 * class; The <code>Catalog</code> allows an instance of the
 * <code>Constraints</code> class to access the characteristics
 * of the constraints around of each end-user-command (which again,
 * is an instance of this class).
 * 
 * @author greg trester
 */
public final class Ask {

  /**
   * A <code>Ask</code> embodies the characteristics of a constraint imposed
   * upon an end-user-activity; additionally, an externality - the context of 
   * ambient weather/temperature - informs the response; NB: the end-user must
   * declare the value of the temperature context [as HOT|COLD] at the outset 
   * of the application's lifecycle; An instance of <code>Ask</code> embodies
   * a single interactive command-line argument; NB: as a nod to practicality,
   * for unit testing - Use Cases/Scenarios - the <code>Ask</code>s are
   * aggregated into a single command-line string, which is then programatically
   * parsed-and-evaluated as a single unit test.
   *
   * @param cmd the commandID
   * @param desc a description of the activity
   * @param hotResp the response to a HOT condition/context
   * @param coldResp the response to a COLD condition/context
   */
  public Ask(int cmd, String desc, String hotResp, String coldResp) {
    commandID = cmd;
    description=  desc;
    hotResponse = hotResp;
    coldResponse = coldResp;
  }

  private final int commandID;
  public int getCommandID() { return commandID; }

  private final String description;
  public String getDescription() { return description; }

  private final String hotResponse;
  public String getHotResponse() { return hotResponse; }

  private final String coldResponse;
  public String getColdResponse() { return coldResponse; }

  
  @Override
  public String toString() {

    StringBuilder model = 
      new StringBuilder(200);

    model.
      append("\n\n commandID ").append(commandID).
      append(" | description ").append(description).
      append(" | hotResponse ").append(hotResponse).
      append(" | coldResponse ").append(coldResponse);

    return model.toString();
  }
}
