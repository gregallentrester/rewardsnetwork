package com.rewardsnetwork.dressing.validation;

import static com.rewardsnetwork.dressing.model.Catalog.FOOTWEAR;
import static com.rewardsnetwork.dressing.model.Catalog.HEADWEAR;
import static com.rewardsnetwork.dressing.model.Catalog.SOCKS;
import static com.rewardsnetwork.dressing.model.Catalog.SHIRT;
import static com.rewardsnetwork.dressing.model.Catalog.JACKET;
import static com.rewardsnetwork.dressing.model.Catalog.PANTS;
import static com.rewardsnetwork.dressing.model.Catalog.LEAVE;
import static com.rewardsnetwork.dressing.model.Catalog.PJOFF;

import com.rewardsnetwork.dressing.exception.RulesViolation;

import com.rewardsnetwork.dressing.model.Ask;
import com.rewardsnetwork.dressing.model.Catalog;

import com.rewardsnetwork.dressing.session.Journal;


/**
 * ßecause there is a fundamental conflict between the formal specs - which
 * state that processing stops-on-error - and the exhibited state-machine
 * conditions (necessary to emit the requisite errata); wherein out-of-order
 * events (as described in the specs) are encountered and yet the event chain
 * must be allowed to continue to satisfy the requisite output (also stipulated
 * in the specs); this means that events (must) continue to be handled beyond
 * the detection of the violation of the specification's mandated ordinality
 * constraints
 *
 * This class has methods which enforce the following business rules/behavioral
 * guidelines:
 * <ul>
 *   <li> 02. PJ’s must be taken off first            [8]                   </li>
 *   <li> 03. Only 1 of each type of clothing                               </li>
 *   <li> 04. No socks [3c] when it is hot            [3c]                  </li>
 *   <li> 05. No jacket [5c] when it is hot           [5c]                  </li>
 *   <li> 06. Socks [3c] before footwear [1]          [3c] -&gt; [1]        </li>
 *   <li> 07. Pants [6c] footwear [1]                 [6c] -&gt; [1]        </li>
 *   <li> 08. Shirt [4] before headwear [2] or jacket [5c][4] &gt; [2],[5c] </li>
 *   <li> 09. Leave only when mandated articles on                          </li>
 * 
 *   <li><strong>
 *   10.  If an invalid command is issued, respond with "FAIL"
 *        and stop processing commands ( fast-fail abend)
 *  </strong>                                                               </li>
 *  
 * </ul>
 *
 * <b>NB</b>
 * This class should ideally be <code>final</code>; however, since <i>Mockito</i>
 *     cannot mock/spy the following linguistic elements, this class remains
 *     non-final:
 * <ul>
 *   <li> final classes     </li>
 *   <li> anonymous classes </li>
 *   <li> primitive types   </li>
 * </ul>
 *
 * <b>NB</b>
 * Inheritance is usually not a good implementation option (instead, as a rule,
 * favor <i>Composition</i>)
 *
 * Finally, the general structure of this validation class avoids two pitfalls
 * of the <i>Arrow Antipattern</i>:
 * <ul>
 *   <li>
 *     Primarily, <i>Cyclomatic Complexity</i>
 *   </li>
 *   <li>
 *     Secondarily, the cognitive dissonance of
 *     right-scrolling (even web pages avoid this)
 *   </li>
 * </ul>
 *
 * The <i>bitly.com</i> link to Cyclomatic Complexity:
 * 
 * @see <a href='https://bit.ly/2fdDqlh'>Cyclomatic Complexity</a>
 *
 * @author greg trester
 */
public class Constraints {
  
  private final Catalog catalog;
  private final Journal journal;

  private boolean isFirstRequest = Boolean.TRUE;
  private boolean isSecondRequest = Boolean.FALSE;
  private boolean isThirdRequest = Boolean.FALSE;

  private static final String HOT =
    Journal.Climate.ValidStates.HOT.name();

  
  private static final String COLD =
    Journal.Climate.ValidStates.COLD.name();

  
  /**
   * The canonical entry-point into a Java app.
   */
  public Constraints() {

    catalog = new Catalog();
    journal = new Journal();
  }


  /**
   * The canonical entry-point into a Java app; this overloaded constructor
   * accommodates <i>TestNG</i> unit tests, the arguments, which are submitted
   * en-batch.
   *
   * @param args an initialization list that is RESERVED FOR
   *             batch submission of Unit-testing scenarios
   */
  public Constraints(String[] args) {
    this();
  }

  
  /**
   * Imposes pre-defined business-rule constraints upon the end-user's dressing
   * session; Which of the <code>assertXxx()</code> validation methods is
   * invoked is based upon the integral value of 'cmdID' (and as stipulated by
   * the specs; Essentially, each <code>assertXxx()</code> validation method
   * possesses 'veto' power over whether the app can proceed with processing -
   * as in <i>Fast-Fail</i> - again, as per an interpretation of the specs;
   *
   * To implement the veto feature, I chose to subclass the canonical class:
   *
   *  <blockquote>RuntimeException</blockquote>
   *
   *  Named:
   *
   *  <blockquote>RulesException</blockquote>
   *
   *  Then, I raise an instance of the error under the appropriate circumstance;
   *
   * <b>NB</b>
   * As a side note, methods don't chain/cascade, as perhaps they might if the
   * <i>ßuilder Pattern</i> was being utilized; This pattern would actually make
   * validation less deterministic, making it more difficult to reason about
   * side-effects (should changes to the order invocation of the <i>ßuilder
   * Pattern</i> methods' occur); however, the arbitrary ordering of methods
   * afforded by the <i>ßuilder Pattern</i> would better afford what-if tests
   * (or to a Chaos-Monkey Resilience Test).
   *
   * @param cmdID the encoded end-user-ask, which is an indice
   *              into a collection of <code>Ask</code>s
   *
   * @see com.rewardsnetwork.demo.session.Console#accept()
   */
  public void handleAsk(String cmdID) throws RulesViolation {

    try {

      if (isFirstRequest) {

        validateClimateState(cmdID);
        
        isFirstRequest = Boolean.FALSE;
        isSecondRequest = Boolean.TRUE;
      }
      else if (isSecondRequest) {

        if (cmdID.equals("0")) {
          journal.addEvent(cmdID);
        }
        else {
          journal.flagErrorState();
          reportJournaledEvents();
          throw new RulesViolation();
        }

        isSecondRequest = Boolean.FALSE;
        isThirdRequest = Boolean.TRUE;
      }
      else if (isThirdRequest) {

        if (cmdID.equals("8")) {
          journal.addEvent(cmdID);
        }
        else {
          journal.flagErrorState();
          reportJournaledEvents();
          throw new RulesViolation();
        }
        
        isThirdRequest = Boolean.FALSE;
      }
      else {

        Ask cmd = catalog.lookup(cmdID);

        switch (cmd.getCommandID()) {

          case FOOTWEAR:  // 1
            validateFootwearWhenHot(cmd);
            break;

          case HEADWEAR:  // 2
            validateHeadwearWhenHot(cmd);
            break;

          case SOCKS:     // 3
            validateSocksOnWhenHot(cmd);
            break;

          case SHIRT:     // 4
            validateShirtOn(cmd);
            break;

          case JACKET:    // 5
            validateJacketWhenHot(cmd);
            break;

          case PANTS:     // 6
            validatePantsOn(cmd);
            break;

          case LEAVE:     // 7
            validateAllArticlesOn(cmd);
            break;

          case PJOFF:     // 8
            validatePJsOff(cmd);
            break;

          default:
            throw new RulesViolation();
        }
      }
    }
    catch (NumberFormatException e) {
      throw new RulesViolation();
    }
  }


  /**
   * Climate must be the first command-line argument with a value of:
   *
   * <ul>
   *  <li>
   *    <code>HOT</code>
   *  </li>
   *  <li>
   *    <code>COLD</code>
   *  </li>
   * </ul>
   *
   * @param climate the relative temperature; enforced as
   *                being supplied by the end-user as the
   *                first command-line argument
   * @see #handleAsk(String cmd)
   */
  public void validateClimateState(String climate) {

    if ( ! climate.equalsIgnoreCase(HOT) &&
         ! climate.equalsIgnoreCase(COLD)) {
      throw new RulesViolation();
    }
    
    journal.setClimate(climate);
  }


  /**
   * You start in the house with your PJ’s on |CMD == 0|.
   *
   * @param cmd the event that provides a context to check 
   *            whether the starting point is the house, 
   *            w/ PJs on
   * 
   * @see #handleAsk(String cmd)
   */
  public final void validateStartInHouseWithPJsOn(Ask cmd) {

    String positedEvent =
      Integer.toString(cmd.getCommandID());

    if (cmd.getCommandID() == PJOFF ||
        journal.publishedEvents().size() > 0) {

      boolean successfullyAdded =
        journal.addEvent(positedEvent);

      if ( ! successfullyAdded) {
        throw new RulesViolation();
      }
    }
  }


  /**  
   * Put on sandals when it is hot; 
   * put on boots when it is cold |CMD == 1|.
   * 
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validateFootwearWhenHot(Ask cmd) {

    if ( ! journal.getClimate().equalsIgnoreCase(HOT)) {

      assureThisHappened(Integer.toString(PANTS));
      assureThisHappened(Integer.toString(SOCKS));
    }

    journal.addEvent(Integer.toString(cmd.getCommandID()));
  }


  /** 
   * Put on sunglasses when it is hot;
   * put on a hat when it is cold |CMD == 2|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validateHeadwearWhenHot(Ask cmd) {

    assureThisHappened(Integer.toString(PJOFF));
    assureThisHappened(Integer.toString(SHIRT));

    journal.addEvent(Integer.toString(cmd.getCommandID()));
  }


  /** 
   * Socks must be put on before footwear
   * (either: socks, pants | pants, socks) |CMD == 3|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validateSocksOnWhenHot(Ask cmd) {

    if ( ! journal.getClimate().equalsIgnoreCase(HOT)) {
      journal.addEvent(Integer.toString(cmd.getCommandID()));
    }
  }


  /** 
   * The shirt must be put on before the headwear or jacket |CMD == 4|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validateShirtOn(Ask cmd) {

    journal.addEvent(Integer.toString(cmd.getCommandID()));
  }


  /**
   * You cannot put on a jacket when it is hot |CMD == 5|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validateJacketWhenHot(Ask cmd) {

    assureThisHappened(Integer.toString(SHIRT));

    if ( ! journal.getClimate().equalsIgnoreCase(HOT)) {
      journal.addEvent(Integer.toString(cmd.getCommandID()));
    }
  }


  /** 
   * Pants must be put on before footwear  |CMD == 6|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validatePantsOn(Ask cmd) {
    journal.addEvent(Integer.toString(cmd.getCommandID()));
  }


  /** 
   * You cannot leave the house until all items of clothing
   * are on (except socks and a jacket when it's hot) |CMD == 7|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validateAllArticlesOn(Ask cmd) {
    journal.addEvent(Integer.toString(cmd.getCommandID()));
  }


  /**
   * 
   * Pajamas must be taken off before anything else can be put on |CMD == 8|.
   *
   * @param cmd signifies an encoded dressing activity
   * @see #handleAsk(String cmd)
   */
  public final void validatePJsOff(Ask cmd) {
    journal.addEvent(Integer.toString(cmd.getCommandID()));
  }


  /**
   * This method is leveraged to impose constraints on certain
   * [events], which must happen before other [events]; all/any
   * [events] are journaled in realtime.
   * For example:
   *
   *  01. Start in the house with your PJ’s on    [0]  -&gt; [8]
   *  02. PJ’s must be taken off first            [8]
   *  03. Only 1 of each type of clothing
   *  04. No socks [3c] when it is hot            [3c]
   *  05. No jacket [5c] when it is hot           [5c]
   *  06. Socks [3c] before footwear [1]          [3c] -&gt; [1]
   *  07. Pants [6c] footwear [1]                 [6c] -&gt; [1]
   *  08. Shirt [4] before headwear [2] or jacket [5c][4]  &gt; [2],[5c]
   *  09. Leave only when mandated articles on  
   *  10. Invalid:  fast-fail abend
   *
   * @param cmdID (indice) of the mandated event that is being
   *              searched-for; this method is called within
   *              the context of another event to make sure
   *              that the stipulated event has occurred
   *
   * @see #handleAsk(String cmd)
   */
  public final void assureThisHappened(String cmdID) {
    
    if ( ! journal.contains(cmdID)) {

      journal.flagErrorState();
      reportJournaledEvents();
      throw new RulesViolation();
    }
  }


  /**
   * Only 1 piece of each type of clothing may be put
   * on (Exception: socks, the unit of which is a pair)
   *
   * @param cmdID (indice) of the mandated event that is being
   *              searched-for; this method is called within
   *              the context of another event to make sure
   *              that a given event is never repeated
   *
   * @see #handleAsk(String cmd)
   * 
   * @Deprecated because there is a fundamental conflict between
   *             the formal specifications - which stipulate that
   *             processing stops-on-error - and the exhibited
   *             state-machine conditions, wherein out-of-order
   *             events (as described in the specs) are encountered
   *             and yet the event chain must continue (to satisfy
   *             the requisite output) beyond the violation of the
   *             specification's mandated ordinality.
   */
  public final void errorOnRepeat(String cmdID) {

    if (journal.contains(cmdID)) {

      journal.flagErrorState();

      reportJournaledEvents();
      throw new RulesViolation();
    }
  }


  /**
   * A delegating/telegraphing call that originates in
   * <code>../session.Console</code>, and delegates to
   * the delegate <code>journal</code> (an instance of
   * <code>Journal</code>).
   */
  public void reportJournaledEvents() {
    journal.publish();
  }
}
