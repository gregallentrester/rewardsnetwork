package com.rewardsnetwork.demo.validation;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import com.rewardsnetwork.dressing.model.Ask;
import com.rewardsnetwork.dressing.model.Catalog;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Animate the <i>TestNG spys</i> (spies) and any collaborating class' methods -
 * and, if no exceptions are raised duing the animation process, declare victory.
 *
 * @author greg trester
 */
public class CatalogNGTest {

  private final String FOOTWEAR = "1";
  private final String HEADWEAR = "2";
  private final String SOCKS = "3";
  private final String SHIRT = "4";
  private final String JACKET = "5";
  private final String PANTS = "6";
  private final String LEAVE = "7";
  private final String PJOFF = "8";


  public CatalogNGTest() {
  }

  @BeforeClass
  public static void setUpClass() throws Exception {
  }

  @AfterClass
  public static void tearDownClass() throws Exception {
  }

  @BeforeMethod
  public void setUpMethod() throws Exception {
  }

  @AfterMethod
  public void tearDownMethod() throws Exception {
  }

  /**
   * Test of <code>lookup</code> method,
   * of class <code>Catalog</code>.
   */
  @Test
  public void testExamineCommandEntry() {

    Catalog catalog = spy(new Catalog());

    // commands are issued as strings @ the command-line
    String cmdkey;
    Ask cmd;

    cmdkey = FOOTWEAR;
    cmd = catalog.lookup(cmdkey);

    assertEquals("Put on footwear", cmd.getDescription());
    assertEquals("sandals", cmd.getHotResponse());
    assertEquals("boots", cmd.getColdResponse());

    cmdkey = HEADWEAR;
    cmd = catalog.lookup(cmdkey);
    assertEquals("Put on headwear", cmd.getDescription());
    assertEquals("sunglasses", cmd.getHotResponse());
    assertEquals("hat", cmd.getColdResponse());
    
    cmdkey = SOCKS;
    cmd = catalog.lookup(cmdkey);
    assertEquals("Put on socks", cmd.getDescription());
    assertEquals("FAIL", cmd.getHotResponse());
    assertEquals("socks", cmd.getColdResponse());

    cmdkey = SHIRT;
    cmd = catalog.lookup(cmdkey);
    assertEquals("Put on shirt", cmd.getDescription());
    assertEquals("shirt", cmd.getHotResponse());
    assertEquals("shirt", cmd.getColdResponse());

    cmdkey = JACKET;
    cmd = catalog.lookup(cmdkey);
    assertEquals("Put on jacket", cmd.getDescription());
    assertEquals("FAIL", cmd.getHotResponse());
    assertEquals("jacket", cmd.getColdResponse());

    cmdkey = PANTS;
    cmd = catalog.lookup(cmdkey);
    assertEquals("Put on pants", cmd.getDescription());
    assertEquals("shorts", cmd.getHotResponse());
    assertEquals("pants", cmd.getColdResponse());

    cmdkey = LEAVE;
    cmd = catalog.lookup(cmdkey);
    assertEquals("leave house", cmd.getDescription());
    assertEquals("leaving house", cmd.getHotResponse());
    assertEquals("leaving house", cmd.getColdResponse());

    cmdkey = PJOFF;
    cmd = catalog.lookup(cmdkey);
    assertEquals("Take off pajamas", cmd.getDescription());
    assertEquals("Removing PJs", cmd.getHotResponse());
    assertEquals("Removing PJs", cmd.getColdResponse());
  }
}
