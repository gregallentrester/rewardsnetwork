-----------------
Setup/Scaffolding
-----------------

Installation:  The ok script assumes that the 'rewardsnetwork.zip' file that contains the complete application has been installed on a Unix-like system, in the directory: ~/$HOME/stage/.

Please run this ßash command to establish that structure:   
    mkdir -b ~/$HOME/stage/rewardsnetwork

    mkdir -b ~/$HOME/stage/rewardsnetwork/  

Deposit (unzip) the 'rewardsnetwork.zip' file so that its contents reside in the directory/folder that you just created.

-------------------------
Design Decisions & Issues
-------------------------

Some of the requisite processing results (processing side-effects) do not agree w/ the processing guidelines. 

Specifically, one mandated behavior is to stop-processing-on-fail ('fast-fail'); however, the example FAIL is allowed to run its course and report the error after processing concludes:

    Input   COLD 0, 8, 6, 3, 4, 2, 5, 7 
    Output  Removing PJs, pants, socks, shirt, hat, jacket, fail
 
Additionally, I have added a (missing) required data-entry step to address the determination of the 'climate' [ HOT | COLD ] into the state machine by mandating that the end-user supply that value.

Likewise, to deterministically conclude processing, I have enhanced the specifications by mandating that the end-user supply the letter 'Q'/'q' to signal the termination of  processing.

The general specifications are as follows:

    Process a list of commands
    Enforce related rules
    Display appropriate output


-------------
Inputs, Tests
-------------

Inputs

   Temperature type   [ HOT | COLD ]
   Comma-separated list of numeric commands

Business Rules, Condensed/Codified

    1, "Put on footwear", "sandals", "boots"
    2, "Put on headwear", "sunglasses", "hat"
    3, "Put on socks", "FAIL", "socks"
    4, "Put on shirt", "shirt", "shirt"
    5, "Put on jacket", "FAIL", "jacket"
    6, "Put on pants", "shorts", "pants"
    7, "leave house", "leaving house", "leaving house"
    8, "Take off pajamas", "Removing PJs", "Removing PJs"

------------
Test Battery
------------

    Two OKS

    Input   HOT, 0, 8, 6, 4, 2, 1, 7 , Q
    Output  Removing PJs, shorts, shirt, sunglasses,
            sandals, leaving house

    Input   COLD, 0, 8, 6, 3, 4, 2, 5, 1, 7 , Q
    Output  Removing PJs, pants, socks, shirt, hat,
            jacket, boots, leaving house


    Four FAILS

    Input   HOT, 0, 8, 6, 6 
    Output  Removing PJs, shorts, FAIL

    Input   HOT, 0, 8, 6, 3 , 7, Q
    Output  Removing PJs, shorts, FAIL

    Input   COLD, 0, 8, 6, 3, 4, 2, 5, 7 , Q
    Output  Removing PJs, pants, socks, shirt, hat, jacket, FAIL

    Input   COLD, 0, 6 , Q
    Output  FAIL

-----------
Constraints
-----------

    01. Start in the house with your PJ’s on        [0]  -> [8]
    02. Pajamas must be taken off before anything       [8]
    03. Only 1 piece of each type of clothing
    04. No socks [3c] when it is hot     [3c]
    05. No jacket [5c] when it is hot  [5c]
    06. Socks [3c] before footwear [1]   [3c] -> [1]
    07. Pants [6c] before footwear [1]   [6c] -> [1]
    08. Shirt [4] before headwear [2] or jacket [5c]                 [4]  -> [2],[5c]
    09. Leave only after adorned w/ required items  
    10. Invalid:                                        Fast-Fail

---------------------
Templatized Examples
---------------------

    SUCCESS

        Input   HOT 0, 8, 6, 4, 2, 1, 7 
        Output  Removing PJs, shorts, shirt, sunglasses,
                sandals, leaving house

        Input   COLD 0, 8, 6, 3, 4, 2, 5, 1, 7 
        Output  Removing PJs, pants, socks, shirt, hat,
                jacket, boots, leaving house

    FAIL

        Input   HOT 0, 8, 6, 6 
        Output  Removing PJs, shorts, fail

        Input   HOT 0, 8, 6, 3 
        Output  Removing PJs, shorts, fail

        Input   COLD 0, 8, 6, 3, 4, 2, 5, 7 
        Output  Removing PJs, pants, socks, shirt, hat, jacket, fail

        Input   COLD 0, 6 
        Output  fail

------------------------------------
Full Specifications and Instructions
------------------------------------

    A Java solution, providing all source, test, and build support files.

    The project structure assumes that this code is production code, and that peers will maintain the code.

    Everything is ZIP'd.

    Primary Criteria

        Only produce code directly related to the implementation
        of the stated problem and business rules.
        
        Use of OO Principles

        Legible, Maintainable code

        Use of recognizable best practices and design patterns

        Compiling, fully working solution

        Adherence to Business rules.


    Secondary Criteria

        The usage and evident knowledge of the tools, utilities,
        frameworks, and methodologies specified in the job description.

        Focus on the primary criteria - simplicity.
        
        
        ~/stage/rewardsnetwork . ok
        
------------------
 Operation, Modelo 
------------------
        
Navigate to the directory: &nbsp;   ~/$HOME/stage/rewardsnetwork/ directory        
        
        source ok
        
        
        Running com.rewardsnetwork.demo.validation.CatalogNGTest
        Configuring TestNG with: org.apache.maven.surefire.testng.conf.TestNG652Configurator@3a4afd8d
        Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.646 sec
        
        Results :
        
        Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
        
        
         Awaiting ...
        
        hot
        0
        8
        6
        4
        2
        1
        7
        Q
        
         Output  Removing PJs shorts shirt sunglasses sandals leaving house
         ...
                